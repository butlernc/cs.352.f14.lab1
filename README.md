Lab 1: All-Pairs
================

The specification for [Lab 1: All-Pairs] is on the [CS.352.F14] website.

Couldn't get our LaTex file to make, errors out on authors but it looks like it should work fine :(

Our test results for our test are in bin/results/back_up_log.txt. 
Our excel document of our graphs and data are in the bin/results/data_w_charts.xlsx
The LaTex document is located in or doc folder.
(The following is located in bin/)
The way our scripts work is first you must run the master.py to create all of the submit and wrapper files.
Then you must run the generate_runner.py to generate the script that will chmod all of the wrapper files and run them.
Then our output of that was directed to the file back_up_log.txt

Group Members
-------------
Noah Butler
Tyler Liddicoat
Alan Ecker

- Peter Bui <buipj@uwec.edu>

[Lab 1: All-Pairs]: http://cs.uwec.edu/~buipj/teaching/cs.352.f14/lab_01_allpairs.html
[CS.352.F14]:	    http://cs.uwec.edu/~buipj/teaching/cs.352.f14/
