package parallel

import (
	"os"
	"testing"

	"allpairs/data"
)

func BenchmarkAllPairsParallel(b *testing.B) {
	stdout := os.Stdout
	os.Stdout = nil
	for i := 0; i < b.N; i++ {
		AllPairsParallel(data.GetChildrenPaths(), true)
	}
	os.Stdout = stdout
}

func ExampleAllPairsParallel() {
	AllPairsParallel(data.GetChildrenPaths(), true)
	// Output:
	// caleb0.jpg caleb0.jpg =  0 (100%)
	// caleb0.jpg caleb1.jpg = 16 ( 75%)
	// caleb0.jpg caleb2.jpg = 14 ( 78%)
	// caleb0.jpg caleb3.jpg = 14 ( 78%)
	// caleb0.jpg twins0.jpg = 15 ( 76%)
	// caleb0.jpg twins1.jpg = 22 ( 65%)
	// caleb0.jpg twins2.jpg = 17 ( 73%)
	// caleb0.jpg twins3.jpg = 17 ( 73%)
	// caleb1.jpg caleb0.jpg = 16 ( 75%)
	// caleb1.jpg caleb1.jpg =  0 (100%)
	// caleb1.jpg caleb2.jpg =  6 ( 90%)
	// caleb1.jpg caleb3.jpg =  4 ( 93%)
	// caleb1.jpg twins0.jpg = 15 ( 76%)
	// caleb1.jpg twins1.jpg = 22 ( 65%)
	// caleb1.jpg twins2.jpg = 13 ( 79%)
	// caleb1.jpg twins3.jpg = 13 ( 79%)
	// caleb2.jpg caleb0.jpg = 14 ( 78%)
	// caleb2.jpg caleb1.jpg =  6 ( 90%)
	// caleb2.jpg caleb2.jpg =  0 (100%)
	// caleb2.jpg caleb3.jpg =  2 ( 96%)
	// caleb2.jpg twins0.jpg = 11 ( 82%)
	// caleb2.jpg twins1.jpg = 16 ( 75%)
	// caleb2.jpg twins2.jpg =  9 ( 85%)
	// caleb2.jpg twins3.jpg = 11 ( 82%)
	// caleb3.jpg caleb0.jpg = 14 ( 78%)
	// caleb3.jpg caleb1.jpg =  4 ( 93%)
	// caleb3.jpg caleb2.jpg =  2 ( 96%)
	// caleb3.jpg caleb3.jpg =  0 (100%)
	// caleb3.jpg twins0.jpg = 11 ( 82%)
	// caleb3.jpg twins1.jpg = 18 ( 71%)
	// caleb3.jpg twins2.jpg = 11 ( 82%)
	// caleb3.jpg twins3.jpg = 11 ( 82%)
	// twins0.jpg caleb0.jpg = 15 ( 76%)
	// twins0.jpg caleb1.jpg = 15 ( 76%)
	// twins0.jpg caleb2.jpg = 11 ( 82%)
	// twins0.jpg caleb3.jpg = 11 ( 82%)
	// twins0.jpg twins0.jpg =  0 (100%)
	// twins0.jpg twins1.jpg =  7 ( 89%)
	// twins0.jpg twins2.jpg =  6 ( 90%)
	// twins0.jpg twins3.jpg =  4 ( 93%)
	// twins1.jpg caleb0.jpg = 22 ( 65%)
	// twins1.jpg caleb1.jpg = 22 ( 65%)
	// twins1.jpg caleb2.jpg = 16 ( 75%)
	// twins1.jpg caleb3.jpg = 18 ( 71%)
	// twins1.jpg twins0.jpg =  7 ( 89%)
	// twins1.jpg twins1.jpg =  0 (100%)
	// twins1.jpg twins2.jpg =  9 ( 85%)
	// twins1.jpg twins3.jpg =  9 ( 85%)
	// twins2.jpg caleb0.jpg = 17 ( 73%)
	// twins2.jpg caleb1.jpg = 13 ( 79%)
	// twins2.jpg caleb2.jpg =  9 ( 85%)
	// twins2.jpg caleb3.jpg = 11 ( 82%)
	// twins2.jpg twins0.jpg =  6 ( 90%)
	// twins2.jpg twins1.jpg =  9 ( 85%)
	// twins2.jpg twins2.jpg =  0 (100%)
	// twins2.jpg twins3.jpg =  2 ( 96%)
	// twins3.jpg caleb0.jpg = 17 ( 73%)
	// twins3.jpg caleb1.jpg = 13 ( 79%)
	// twins3.jpg caleb2.jpg = 11 ( 82%)
	// twins3.jpg caleb3.jpg = 11 ( 82%)
	// twins3.jpg twins0.jpg =  4 ( 93%)
	// twins3.jpg twins1.jpg =  9 ( 85%)
	// twins3.jpg twins2.jpg =  2 ( 96%)
	// twins3.jpg twins3.jpg =  0 (100%)
}
