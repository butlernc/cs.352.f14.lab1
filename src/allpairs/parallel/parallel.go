package parallel

import (
	m "allpairs/metric"
	u "allpairs/utils"
)

type ResultPair struct {
	Path string
	Hash uint64
}

// Perform All-Pairs comparison of imaages using goroutines.
func AllPairsParallel(paths []string, all bool) {
	hashes := make(map[string]uint64)
	results := make(chan ResultPair)

	for _,p := range paths {
		go func(p string) {
			results <- ResultPair{p, m.ComputePerceptualHash(p)}
		}(p)
	}

	for i := 0; i < len(paths); i++ {
		pair := <-results
		hashes[pair.Path] = pair.Hash
	}

	close(results)

	for p := range u.GenerateCombinations(paths, all) {
		distance := m.ComputeHammingDistance(hashes[p.First], hashes[p.Second])
		m.PrintHammingDistance(p.First, p.Second, distance)
	}
}
