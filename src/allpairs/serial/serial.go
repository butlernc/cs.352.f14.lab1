package serial

import (
	m "allpairs/metric"
	u "allpairs/utils"
)

// Perform All-Pairs comparison of images.
func AllPairsSerial(paths []string, all bool) {
	hashes := make(map[string]uint64)

	for _,p := range paths {
		hashes[p] = m.ComputePerceptualHash(p)
	}

	for p := range u.GenerateCombinations(paths, all) {
		distance := m.ComputeHammingDistance(hashes[p.First], hashes[p.Second])
		m.PrintHammingDistance(p.First, p.Second, distance)
	}
}
