package distributed

import (
	"fmt"
	"io"
	"os"
	"path"
	"strings"

	m "allpairs/metric"
	u "allpairs/utils"
	"workqueue"
)

// Perform All-Pairs comparison of images using WorkQueue.
func AllPairsDistributed(paths []string, all bool, size int) {
	//Create a Work Queue Master, enable CATALOG mode, and set the project
	//name from the "WORK_QUEUE_NAME" environment variable.
	wq := workqueue.NewWorkQueue(workqueue.RANDOM_PORT)
	wq.SpecifyMode(workqueue.CATALOG)
	wq.SpecifyName(os.Getenv("WORK_QUEUE_NAME"))
	//Set Work Queue debug flag to "WORK_QUEUE_DEBUG" environment variable.
	workqueue.SetDebugFlag(os.Getenv("WORK_QUEUE_DEBUG"))

	//Determine path to bruteforce executable
	allpairs_path := u.FindAllPairsExecutable()

	//Iterate over chunks of input paths. For each chunk, schedule a
	//task that computes the hash of each file in the chunk.
	for c := range u.GenerateChunks(paths, size) {
		bases := []string{}
		for _,f := range c {
			bases = append(bases, path.Base(f))
		}
		tcmd := "./allpairs -method=hash " + strings.Join(bases, " ")
		task := workqueue.NewTask(tcmd)
		task.SpecifyInputFile(allpairs_path, "allpairs", workqueue.CACHED)

		for i := 0; i < len(c); i++ {
			task.SpecifyInputFile(c[i], bases[i], workqueue.CACHED)
		}

		wq.Submit(task)
	}

	//Wait for all tasks to complete. For each returned task, check
	//output, and scan output for hash values.
	hashes := make(map[string]uint64)

	for !wq.Empty() {
		task := wq.Wait(workqueue.WAIT_FOR_TASK)
		if task == nil {
		}else{
			reader := strings.NewReader(task.Output())

			var file string
			var hash uint64

			_,err := fmt.Fscanf(reader, "%s %d", &file, &hash)

			for err != io.EOF {
				hashes[file] = hash
				if err == io.EOF {
					break
				}
				 _,err = fmt.Fscanf(reader, "%s %d", &file, &hash)

			}

			task.Delete()
		}
	}

	//Compare all pair-wise combinations of hashes and print results.
	for p := range u.GenerateCombinations(paths, all) {
		distance := m.ComputeHammingDistance(hashes[path.Base(p.First)], hashes[path.Base(p.Second)])
		m.PrintHammingDistance(p.First, p.Second, distance)
	}

}
