package metric

import (
	"fmt"
	"path"
)

// Compute the hamming distance between two Perceptual Hashes.
func ComputeHammingDistance(phash0, phash1 uint64) uint64 {
	var count uint64 = 0
	xored := phash0 ^ phash1
	for ; xored > 0; count++ {
		xored &= xored - 1
	}

	return count
}

// Print hamming distance of two images.
func PrintHammingDistance(path0, path1 string, hamming uint64) {
	fmt.Printf("%s %s = %2v (%3v%%)\n", path.Base(path0), path.Base(path1), hamming, (hashPixels-hamming)*100/hashPixels)
}

// Compute the hamming distances between two image files.
func ComputeHammingDistanceFromFiles(file0, file1 string) {
	hash0 := make(chan uint64)
	hash1 := make(chan uint64)

	go func(f string) {
		hash0<-ComputePerceptualHash(f)
	}(file0)

	go func(f string) {
		hash1<-ComputePerceptualHash(f)
	}(file1)

	distance := ComputeHammingDistance(<-hash0, <-hash1)
	PrintHammingDistance(file0, file1, distance)
}
