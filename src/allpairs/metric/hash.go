package metric

import (
	"image/color"
	"fmt"
	"path"
	"github.com/disintegration/imaging"
	"github.com/harrydb/go/img/grayscale"
)

const (
	hashWidth  = 8
	hashHeight = 8
	hashPixels = hashWidth * hashHeight
)

// Compute the Perceptual Hash of an Image.
func ComputePerceptualHash(path string) uint64 {
	// Open image for processing
	image, err := imaging.Open(path)
	if err != nil{
		panic(err)
	}
	// Resize image to hashWidth x hashHeight using BSpline
	small_image := imaging.Resize(image, hashWidth, hashHeight, imaging.BSpline)
	// Reduce color to grayscale using ToGrayLuminance
	grayscale_image := grayscale.Convert(small_image, grayscale.ToGrayLuminance)
	// Compute the average of the color values
	total := 0
	for row := 0; row < hashHeight; row++ {
		for column := 0; column < hashWidth; column++ {
			pixel := grayscale_image.At(row, column).(color.Gray)
			total += int(pixel.Y)
		}
	}

	average := float64(total)/float64(hashPixels)
	// Compute the hash by setting the bits of a uint64 based on if color
	// value is above or below the means
	var hash uint64
	for row := 0; row < hashHeight; row++ {
		for column := 0; column < hashWidth; column++ {
			hash <<= 1
			pixel := grayscale_image.At(row, column).(color.Gray)
			if float64(pixel.Y) < average {
				hash |= 1
			}
		}
	}
	return hash
}

// Computer Perceptual Hashes for specified files
func ComputePerceptualHashesFromFiles(paths []string) {

	//make our channel for sync check
	done := make(chan bool)

	for _,p := range paths {
		go func(p string, pHash uint64) {
			fmt.Printf("%s %v\n", path.Base(p),pHash)
			done <- true
		}(p,ComputePerceptualHash(p))
	}
	//check to make sure we get the correct number of bools from the channel
	for i :=0; i<len(paths); i++ {
		<-done
	}

	close(done)
}
