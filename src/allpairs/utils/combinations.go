package utils

// Pair of  values.
type Pair struct {
	First, Second string
}

// Generate combinations of pairs from given set of strings.
func GenerateCombinations(strings []string, all bool) <-chan Pair {
	//make our Pair's channel
	pair_chan := make(chan Pair)
	//create our go routine
	go func() {

		for i, first := range strings {
			var rest []string
			if all {
				rest = strings
			} else {
				rest = strings[i:]
			}

			for _, second := range rest {
				pair_chan <- Pair{first, second}
			}
		}
		//close our channel
		close(pair_chan)
	}()
	return pair_chan

}

const DefaultChunkSize = 8 // Default Chunk Size

// Generate chunks of fixed-size from input slice of strings.
func GenerateChunks(strings []string, size int) <-chan []string {

	//make our channel
	string_chan := make(chan []string)

	go func() {
		chunk := []string{}
		for _, s := range strings {
			chunk = append(chunk, s)

			if len(chunk)%size == 0 {
				string_chan <- chunk
				chunk = []string{}
			}
		}

		if len(chunk) > 0 {
			string_chan <- chunk
		}
		//close our channel
		close(string_chan)

	}()

	return string_chan
}
