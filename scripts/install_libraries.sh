#!/bin/sh

[ -z "${GOROOT}" ] && [ -r scripts/env.sh ] && . scripts/env.sh

LIBRARIES="
    github.com/disintegration/imaging
    github.com/harrydb/go/img/grayscale
"

for library in ${LIBRARIES}; do
    echo -n "Installing ${library} ... "
    if go get -u ${library}; then
    	echo "Success!"
    else
    	echo "Fail!"
    fi
done
