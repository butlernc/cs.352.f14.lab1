#!/bin/sh

[ -z "${GOPATH}" ] && [ -r scripts/env.sh ] && . scripts/env.sh

IMAGES="
    caleb0.jpg
    caleb1.jpg
    caleb2.jpg
    caleb3.jpg
    twins0.jpg
    twins1.jpg
    twins2.jpg
    twins3.jpg
"

DATA=${GOPATH}/data

[ ! -d ${DATA} ] && mkdir -p ${DATA}

for image in ${IMAGES}; do
    echo "Fetching ${image}..."
    curl http://cs.uwec.edu/~buipj/teaching/cs.352.f14/static/img/${image} > ${DATA}/${image}
done
