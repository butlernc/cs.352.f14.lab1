#!/usr/bin/env python

from subprocess import call

def run_parallel():
	for runs in range(0,5):
		for cores in range(1,5):
			print ("chmod +x parallel_wrapper%d_%d.sh" % (runs, cores))
			print ("chmod +x serial_wrapper%d_%d.sh" % (runs, cores))
			print ("echo \"PARALLEL GROUP:%d CORES:%d\"" % (runs, cores))
			print ("./parallel_wrapper%d_%d.sh" % (runs, cores))
			print ("echo \"SERIAL GROUP:%d CORES:%d\"" % (runs, cores))
			print ("./serial_wrapper%d_%d.sh" % (runs, cores))
			
run_parallel()
