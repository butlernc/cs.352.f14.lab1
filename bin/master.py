#!/usr/bin/env python

from subprocess import call

def make_file(name, runs, is_parallel):
	
	for cores in range(1, 5):
	
		#create the shell script wrapper
		wrapper_file = "%s_wrapper%d_%d.sh" % (name, runs, cores)
		f = open(wrapper_file, 'w')
		
		if is_parallel:
			f.write(("#!/bin/sh \n env GOMAXPROCS=%d ./allpairs -all=false -method=%s -timed=true /data/users/butlernc/cs.352.f14.lab1/bin/images/group%d/*.jpg" % (cores, name, runs + 1)))
		else:
			f.write(("#!/bin/sh \n ./allpairs -all=false -method=%s -timed=true /data/users/butlernc/cs.352.f14.lab1/bin/images/group%d/*.jpg" % (name, runs + 1)))
		
		f.close()
		
		
		#create submit script for shell script wrapper
		file_name = "%s%d_%d.submit" % (name, runs, cores)
		t = open(file_name, 'w')
		t.write((
		"executable              = ./%s \n" % (wrapper_file) +
		"arguments               = \n" +
		"transfer_input_files    = ./%s,./allpairs \n" % (wrapper_file) +
		"should_transfer_files   = yes \n" +
		"when_to_transfer_output = on_exit \n" +
		"output                  = output/%s_%d_%d.output \n" % (name, runs, cores) +
		"error                   = error/%s_%d_%d.error \n" % (name, runs, cores)  +
		"log                     = log/%s_%d_%d.log \n" % (name, runs, cores)  +
		"getenv                  = true \n" +
		"queue \n"	
		))
		
		t.close()

#make test groups for each number of processors
for i in range(0, 5):
	make_file("serial", i, False)
	make_file("parallel", i, True)
	